﻿using Dapper.Contrib.Extensions;
using System;

namespace PDI.Automation.Envoy.Database
{
    [Table("Language_Value")]
    public class LanguageValue
    {
        public string Language_Code { get; set; }
        public string Key_Text { get; set; }
        public int Key_ID { get; set; }
        public string Value { get; set; }
    }
}

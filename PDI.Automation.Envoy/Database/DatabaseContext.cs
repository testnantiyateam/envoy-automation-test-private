﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace PDI.Automation.Envoy.Database
{
    public class DatabaseContext : IDisposable
    {
        private SqlConnection _connection;
        public SqlConnection Connection
        {
            get
            {
                if (_connection != null && _connection.State == ConnectionState.Closed)
                {
                    _connection.Open();
                }
                return _connection;
            }
            set
            {
                if (_connection != null && _connection.State == ConnectionState.Open)
                {
                    _connection.Close();
                }
                _connection = value;
            }
        }

        public DatabaseContext(string server, string database, string userID, string password)
        {
            //Important for not including underscores in the model
            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;

            var connectionString = new SqlConnectionStringBuilder()
            {
                DataSource = server,
                InitialCatalog = database,
                UserID = userID,
                Password = password
            };

            Connection = new SqlConnection(connectionString.ToString());
        }

        void IDisposable.Dispose()
        {
            Connection.Close();
            Connection.Dispose();
        }
    }
}

﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace PDI.Automation.Envoy.Database
{
    public class Translations
    {
        public List<LanguageValue> translations;

        //Collect all translation strings of a specific language and store in translations List.
        public Translations(SqlConnection sqlConnection, string language)
        {
            translations = sqlConnection.Query<LanguageValue>
            (@"Select * from Language_Master 
	                    inner join Language_Value
		                on Key_ID = RID and Language_Code = @Language", new { Language = language }).ToList();
        }

        //From the collection get the translated string value.
        public string Translate(string value)
        {
            LanguageValue translation = translations.Where(x => x.Key_Text == value).FirstOrDefault();
            return translation != null ? translation.Value : value;
        }
    }
}

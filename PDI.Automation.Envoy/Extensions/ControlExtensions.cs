﻿using FlaUI.Core.Input;
using FlaUI.Core.WindowsAPI;
using PDI.Automation.Envoy.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlaUI.Core.AutomationElements;
using System.Resources;
using PDI.Automation.WinForms.Controls;

namespace PDI.Automation.Envoy.Extensions
{
    public static class ControlExtensions
    {

        public static void EnterGLAccount(this PDIDataGridCell cell, string accountNumber)
        {
            var accountParts = accountNumber.Split('-');

            cell.Click();

            foreach (var part in accountParts)
            {
                Keyboard.Type(part);
                Wait.UntilInputIsProcessed();

                if (part != accountParts.Last())
                    Keyboard.Type(VirtualKeyShort.TAB);
            }
        }

        public static void Enter(this TextBox textBox, DateTime value)
        {
            textBox.Enter(value.ToShortDateString());
        }

        public static void Enter(this PDIDate date, DateTime value)
        {
            date.Enter(value.ToShortDateString());
        }

        //To Get Hidden Window Text value from Pane object
        public static string GetWindowText(this Pane pane)
        {
            return Win32.GetHiddenWindowText(pane);
        }

        //To Get Hidden Window Text value from Automation Element
        public static string GetWindowText(this AutomationElement element)
        {
            Pane pane = new Pane(element);
            return Win32.GetHiddenWindowText(pane);
        }

        //To get Center point of an object.
        public static void GetCenterPoint(this AutomationElement element, out int x, out int y)
        {
            var RectangleObj = element.BoundingRectangle;

            x = (2 * RectangleObj.X + RectangleObj.Width) / 2;
            y = (2 * RectangleObj.Y + RectangleObj.Height) / 2;
        }

        //To get required Tab point
        public static void GetRequiredTabItemPoint(this Pane element, int RequiredTabPointIndex, int TotalTabsCount, out int x, out int y)
        {
            var RectangleObj = element.BoundingRectangle;

            x = RectangleObj.X + (RectangleObj.Width / (TotalTabsCount + 1)) * RequiredTabPointIndex;
            y = RectangleObj.Y;
        }
    }
}

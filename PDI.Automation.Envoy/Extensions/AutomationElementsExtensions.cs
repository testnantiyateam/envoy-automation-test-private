﻿using FlaUI.Core.AutomationElements;
using PDI.Automation.Envoy.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDI.Automation.Envoy.Extensions
{
    public static class AutomationElementsExtensions
    {
        public static PDIDataGrid AsPDIDataGrid(this AutomationElement self)
        {
            return self == null ? null : new PDIDataGrid(self);
        }

        public static PDIDatePicker AsPDIDatePicker(this AutomationElement self)
        {
            return self == null ? null : new PDIDatePicker(self);
        }

        public static PDIDate AsPDIDate(this AutomationElement self)
        {
            return self == null ? null : new PDIDate(self);
        }
    }
}

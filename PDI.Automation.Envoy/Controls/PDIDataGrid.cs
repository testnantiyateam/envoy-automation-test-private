﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlaUI.Core.Conditions;
using FlaUI.Core.Definitions;
using FlaUI.Core.Exceptions;
using FlaUI.Core.WindowsAPI;
using FlaUI.Core.AutomationElements;
using FlaUI.Core;
using FlaUI.Core.Tools;
using FlaUI.Core.Patterns;
using FlaUI.Core.Input;
using NUnit.Framework;
using PDI.Automation.WinForms.Controls;
using PDI.Automation.Envoy.Extensions;

namespace PDI.Automation.Envoy.Controls
{
    /// <summary>
    /// Class to interact with a Enterprise PDIDataGrid
    /// </summary>
    public class PDIDataGrid : AutomationElement
    {
        /// <summary>
        /// Creates a <see cref="PDIDataGrid"/> element.
        /// </summary>
        /// 

        public PDIDataGrid(AutomationElement element) : base(element.FrameworkAutomationElement)
        {
            
        }

        /// <summary>
        /// Gets all the data rows.
        /// </summary>
        public virtual bool HasAnyRow
        {
            get
            {
                bool returnValue = false;

                //Check the total number of pane present under PDIDataGrid, if it has only 1 pane (vertical scrool bar), then there will be no rows/cells.
                int cellCount = FindAllChildren(cf => cf.ByControlType(ControlType.Pane)).Count() - 1;
                if (cellCount > 0)
                {
                    returnValue = true;
                }
                return returnValue;
            }
        }


        public virtual int GetRowCount(string uniqueColumnName)
        {
            return ListRows(uniqueColumnName).Count;
        }

        /// <summary>
        /// Gets first row data.
        /// </summary>
        public virtual PDIDataGridCell[] GetFirstRowCells()
        {
            //Click the grd table to select first row from the table.
            Click();

            List<PDIDataGridCell[]> Allrows = new List<PDIDataGridCell[]>();

            if (HasAnyRow)
            {

                var cells = FindAllChildren(cf => cf.ByControlType(ControlType.Pane)).ToArray();
                Allrows.Add(cells.Skip(1).ToArray().Select(x => new PDIDataGridCell(x)).ToArray());

                return Allrows.First();
            }
            else
                return null;
        }

        /// <summary>
        /// Gets all the data rows.
        /// </summary>
        public virtual List<PDIDataGridCell[]> ListRows(string uniqueColumnName)
        {
            //Click the grd table to select first row from the table.
            Click();
            VirtualKeyShort[] keys = { VirtualKeyShort.CONTROL, VirtualKeyShort.HOME };

            //Send the Ctrl + Home key to move to the first element
            Keyboard.TypeSimultaneously(keys);

            List<PDIDataGridCell[]> Allrows = new List<PDIDataGridCell[]>();

            if (HasAnyRow)
            {
                bool isLastRow = false;
                string firstRowValue = "test";

                while (!isLastRow)
                {

                    var columnName = FindFirstDescendant(cf => cf.ByName(uniqueColumnName));
                    var childPane = columnName.FindFirstDescendant(cf => cf.ByControlType(ControlType.Pane));
                    string nextRowValue = childPane.GetWindowText();

                    if (firstRowValue.Equals(nextRowValue) || string.IsNullOrEmpty(nextRowValue))
                    {
                        isLastRow = true;
                    }
                    else
                    {
                        var cells = FindAllChildren(cf => cf.ByControlType(ControlType.Pane)).ToArray();
                        Allrows.Add(cells.Skip(1).ToArray().Select(x => new PDIDataGridCell(x)).ToArray());

                        firstRowValue = nextRowValue;
                        Keyboard.Type(VirtualKeyShort.DOWN);
                    }
                }

                //Send the Ctrl + Home key to move to the first element
                Keyboard.TypeSimultaneously(keys);
                Wait.UntilInputIsProcessed();

                return Allrows;
            }
            else
                return null;
        }

        /// <summary>
        /// Gets all the data rows.
        /// </summary>
        public virtual List<string> GetAllColumnValues(string uniqueColumnName)
        {
            List<string> AllColumnValues = new List<string>();

            //Click the grd table to select first row from the table.
            Click();
            VirtualKeyShort[] keys = { VirtualKeyShort.CONTROL, VirtualKeyShort.HOME };

            //Send the Ctrl + Home key to move to the first element
            Keyboard.TypeSimultaneously(keys);

            List<PDIDataGridCell[]> Allrows = new List<PDIDataGridCell[]>();

            if (HasAnyRow)
            {
                bool isLastRow = false;
                string firstRowValue = "test";

                while (!isLastRow)
                {

                    var columnName = FindFirstDescendant(cf => cf.ByName(uniqueColumnName));
                    var childPane = columnName.FindFirstDescendant(cf => cf.ByControlType(ControlType.Pane));
                    string nextRowValue = childPane.GetWindowText();

                    if (firstRowValue.Equals(nextRowValue) || string.IsNullOrEmpty(nextRowValue))
                    {
                        isLastRow = true;
                    }
                    else
                    {
                        AllColumnValues.Add(nextRowValue);

                        firstRowValue = nextRowValue;
                        Keyboard.Type(VirtualKeyShort.DOWN);
                    }
                }

                //Send the Ctrl + Home key to move to the first element
                Keyboard.TypeSimultaneously(keys);
                Wait.UntilInputIsProcessed();

                return AllColumnValues;
            }
            else
                return null;
        }

        /// <summary>
        /// Select the specific row.
        /// </summary>
        public virtual void SelectRow(string uniqueColumnName, string columnValue)
        {
            //Click the grd table to select first row from the table.
            Click();
            VirtualKeyShort[] keys = { VirtualKeyShort.CONTROL, VirtualKeyShort.HOME };

            //Send the Ctrl + Home key to move to the first element
            Keyboard.TypeSimultaneously(keys);

            List<PDIDataGridCell[]> Allrows = new List<PDIDataGridCell[]>();

            bool isLastRow = false;
            if (HasAnyRow)
            {
                string firstRowValue = "test";
                while (!isLastRow)
                {
                    var columnName = FindFirstDescendant(cf => cf.ByName(uniqueColumnName));
                    var childPane = columnName.FindFirstDescendant(cf => cf.ByControlType(ControlType.Pane));

                    TextBox edit = childPane.FindFirstDescendant(cf => cf.ByControlType(ControlType.Edit))?.AsTextBox();

                    //For 2nd iteration we don't have to click the childpane to get the text value
                    if (edit is null)
                    {
                        childPane.WaitUntilClickable();
                        childPane.Click();
                        edit = childPane.FindFirstDescendant().AsTextBox();
                    }

                    //Validate the column value is matching, if not Pressing the Down array key to get the next row value to check.
                    string nextRowValue = edit.Text;

                    if(nextRowValue.Equals(columnValue,StringComparison.OrdinalIgnoreCase))
                    {
                        break;
                    }

                    if (firstRowValue.Equals(nextRowValue) || string.IsNullOrEmpty(nextRowValue))
                    {
                        isLastRow = true;
                    }
                    else
                    {
                        var cells = FindAllChildren(cf => cf.ByControlType(ControlType.Pane)).ToArray();
                        Allrows.Add(cells.Skip(1).ToArray().Select(x => new PDIDataGridCell(x)).ToArray());

                        firstRowValue = nextRowValue;
                        Keyboard.Type(VirtualKeyShort.DOWN);
                    }
                }
            }
            else
                Assert.Fail("SelectRow : No rows present to select the value");

            if(isLastRow)
                Assert.Fail("SelectRow : " + columnValue + " does not exist in the column " + uniqueColumnName);
        }


        /// <summary>
        /// Select the specific row.
        /// </summary>
        public virtual string GetSelectedValue(string uniqueColumnName)
        {
            if (HasAnyRow)
            {
                    var columnName = FindFirstDescendant(cf => cf.ByName(uniqueColumnName));
                    var childPane = columnName.FindFirstDescendant(cf => cf.ByControlType(ControlType.Pane));

                    TextBox edit = childPane.FindFirstDescendant(cf => cf.ByControlType(ControlType.Edit))?.AsTextBox();

                    //For 2nd iteration we don't have to click the childpane to get the text value
                    if (edit is null)
                    {
                        childPane.WaitUntilClickable();
                        childPane.Click();
                        edit = childPane.FindFirstDescendant().AsTextBox();
                    }
                    return edit.Text;
            }
            else
                Assert.Fail("SelectRow : No rows present to select the value");

            return null;
        }

    }

    /// <summary>
    /// Class to interact with a Enterprise PDIDataGrid cell.
    /// </summary>
    public class PDIDataGridCell : AutomationElement
    {
        /// <summary>
        /// Creates a <see cref="PDIDataGridCell"/> element.
        /// </summary>
        public PDIDataGridCell(AutomationElement element) : base(element.FrameworkAutomationElement)
        {

        }

        /// <summary>
        /// Pattern object for the <see cref="IValuePattern"/>.
        /// </summary>
        protected IValuePattern ValuePattern => Patterns.Value.Pattern;

        /// <summary>
        /// Gets or sets the value in the cell.
        /// </summary>
        public string Value
        {
            get
            {

                var childPane = FindFirstDescendant(cf => cf.ByControlType(ControlType.Pane));

                return childPane.GetWindowText();

            }
        }

        /// <summary>
        /// Simulate typing in text to the cell.
        /// </summary>
        public void Enter(string value)
        {
            //Click to expose the control
            Click();

            //Find the actual edit control in the view - 2nd parent should be the grid
            var edit = Parent.Parent.FindFirstDescendant(cf => cf.ByControlType(ControlType.Edit));

            //Get the value pattern for the edit control
            var valuePattern = edit.Patterns.Value.PatternOrDefault;

            valuePattern?.SetValue(String.Empty);
            if (String.IsNullOrEmpty(value)) return;

            Keyboard.Type(value);

            Wait.UntilInputIsProcessed();
        }
    }
}

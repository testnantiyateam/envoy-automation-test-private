﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlaUI.Core.AutomationElements;
using FlaUI.Core.Input;
using FlaUI.Core.WindowsAPI;
using FlaUI.Core.Definitions;

namespace PDI.Automation.Envoy.Controls
{
    public class PDIDatePicker : ComboBox
    {
        public PDIDatePicker(AutomationElement element) : base(element.FrameworkAutomationElement)
        {
        }

        public void SelectDate(DateTime value)
        {
            Click();
            ComboBoxItem DateItems = Items.ToList().Where(x => x.Name.Equals("Select Date...", StringComparison.OrdinalIgnoreCase)).First();
            DateItems.Click();
            Keyboard.Type(value.ToShortDateString());
            Keyboard.Type(VirtualKeyShort.TAB);
        }

        public void SelectToday()
        {
            Click();
            ComboBoxItem DateItems = Items.ToList().Where(x => x.Name.Equals("Today", StringComparison.OrdinalIgnoreCase)).First();
            DateItems.Click();
        }

        public void SelectYesterday()
        {
            Click();
            ComboBoxItem DateItems = Items.ToList().Where(x => x.Name.Equals("Yesterday", StringComparison.OrdinalIgnoreCase)).First();
            DateItems.Click();
        }
    }
}

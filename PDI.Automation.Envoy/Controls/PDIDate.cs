﻿using FlaUI.Core.AutomationElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlaUI.Core.Definitions;
using FlaUI.Core.Input;
using FlaUI.Core.WindowsAPI;

namespace PDI.Automation.Envoy.Controls
{
    /// <summary>
    /// Class to interact with a Enterprise PDIDate
    /// </summary>
    public class PDIDate : AutomationElement
    {
        /// <summary>
        /// Creates a <see cref="PDIDate"/> element.
        /// </summary>
        public PDIDate(AutomationElement element) : base(element.FrameworkAutomationElement)
        {
        }

        /// <summary>
        /// Simulate typing in text.
        /// </summary>
        public void Enter(string value)
        {
            Focus();

            //Find the actual edit control in the view
            var edit = FindFirstDescendant(cf => cf.ByControlType(ControlType.Edit));

            //Get the value pattern for the edit control
            var valuePattern = edit.Patterns.Value.PatternOrDefault;

            valuePattern?.SetValue(String.Empty);
            if (String.IsNullOrEmpty(value)) return;

            var lines = value.Replace("\r\n", "\n").Split('\n');
            Keyboard.Type(lines[0]);
            foreach (var line in lines.Skip(1))
            {
                Keyboard.Type(VirtualKeyShort.RETURN);
                Keyboard.Type(line);
            }
            Wait.UntilInputIsProcessed();
        }
    }
}

﻿using FlaUI.Core.AutomationElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlaUI.Core.Definitions;
using PDI.Automation.Envoy.Controls;
using PDI.Automation.Envoy.Extensions;
using FlaUI.Core.Input;
using System.Threading;
using PDI.Automation.WinForms.Extensions;
using PDI.Automation.Envoy.Database;

namespace PDI.Automation.Envoy.Modules
{
    public class ImportFromScanner : EnvoyHome
    {

        public ImportFromScanner(Window window, Translations Translations) : base(window, Translations)
        {

        }

        //Import from Scanner Objects
        TextBox adjustmentDate => Window.FindFirstDescendant(x => x.ByAutomationId("TextBox")).AsTextBox();
        TextBox inventoryCountDate => Window.FindFirstDescendant(x => x.ByControlType(ControlType.Edit)).AsTextBox();

        //Import Files Preparation Objects
        Window importFilesPreparationWindow => Window.FindFirstDescendantWithRetry(x => x.ByName(Translations.Translate("Import Files Preparation"))).AsWindow();
        Window PrintPreviewFormExBaseWindow => Window.FindFirstDescendant(x => x.ByAutomationId("PrintPreviewFormExBase")).AsWindow();
        Button OkButton => FindFirstDescendant(cf => cf.ByAutomationId("btnOk"))?.AsButton();

        Window WACReportPreviewWindow => Window.Automation.GetDesktop().FindFirstDescendantWithRetry(x => x.ByAutomationId("PrintPreviewFormExBase"), TimeSpan.FromMilliseconds(1000))?.AsWindow();

        //Import the file that was copied into Input Directory ("C:\EnvoyCoFolders\PDI\Scanner\Import")
        public void Import(ImportFilePreparation importDateObj)
        {
            ClickOK();
            Wait.UntilInputIsProcessed();
            adjustmentDate.Enter(importDateObj.AdjustmentDate.ToString("dd MMM yyyy"));
            Wait.UntilInputIsProcessed();
            //Sometimes inventoryCountDate would be non editable
            if (inventoryCountDate != null && inventoryCountDate.IsEnabled)
                inventoryCountDate.Enter(importDateObj.InventoryCountDate.ToString("dd MMM yyyy"));
            ClickOKButton();
            Thread.Sleep(1500);
            if (OkButton != null)
                OkButton.Click();
        }

        public void AcknowledgeMessage()
        {
            ClickOK();
            if (WACReportPreviewWindow != null)
                WACReportPreviewWindow.Close();
            //Click cancel button after the import
            ClickCancelButton();
        }

        public void ClickCancelButton()
        {
            var collection = importFilesPreparationWindow.FindAllDescendants(x => x.ByControlType(ControlType.Button).And(x.ByName("")));
            collection.First().AsButton().Click();
        }

        public void ClickOKButton()
        {
            var collection = importFilesPreparationWindow.FindAllDescendants(x => x.ByControlType(ControlType.Button).And(x.ByName("")));
            collection.Last().AsButton().Click();
        }
    }
}

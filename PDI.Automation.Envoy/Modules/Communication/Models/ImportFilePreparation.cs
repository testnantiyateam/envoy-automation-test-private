﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDI.Automation.Envoy.Modules
{
    public class ImportFilePreparation
    {
        public DateTime AdjustmentDate { get; set; }
        public DateTime InventoryCountDate { get; set; }
    }
}

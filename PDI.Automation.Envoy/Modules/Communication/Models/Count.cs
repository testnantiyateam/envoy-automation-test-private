﻿using FixedWidthParserWriter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDI.Automation.Envoy.Modules
{
    public class Count
    {
        [FixedWidthLineField(Start = 1, Length = 15)]
        public string UPCCode { get; set; }

        [FixedWidthLineField(Start = 16, Length = 3)]
        public string UOM { get; set; }

        [FixedWidthLineField(Start = 19, Length = 7)]
        public string Quantity { get; set; }

        [FixedWidthLineField(Start = 26, Length = 6)]
        public string Time { get; set; }
    }
}

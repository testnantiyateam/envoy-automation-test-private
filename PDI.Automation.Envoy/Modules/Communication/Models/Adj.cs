﻿using FixedWidthParserWriter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDI.Automation.Envoy.Modules
{
    public class Adj
    {
        [FixedWidthLineField(Start = 1, Length = 15)]
        public string UPCCode { get; set; }

        [FixedWidthLineField(Start = 16, Length = 7)]
        public string Quantity { get; set; }

        [FixedWidthLineField(Start = 23, Length = 5)]
        public string AdjustmentType { get; set; }

        [FixedWidthLineField(Start = 28, Length = 8)]
        public string Remark { get; set; }

        [FixedWidthLineField(Start = 36, Length = 6)]
        public string AdjustTime { get; set; }

        [FixedWidthLineField(Start = 42, Length = 3)]
        public string UOM { get; set; }

        [FixedWidthLineField(Start = 45, Length = 1)]
        public string AddOrRemoveFlag { get; set; }

    }
}

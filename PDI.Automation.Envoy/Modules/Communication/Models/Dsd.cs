﻿using FixedWidthParserWriter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDI.Automation.Envoy.Modules
{
    public class Dsd
    {
        [FixedWidthLineField(Start = 1, Length = 8, Format = "ddMMyyyy")]
        public DateTime InvoiceDate { get; set; }

        [FixedWidthLineField(Start = 9, Length = 15)]
        public string InvoiceNumber { get; set; }

        [FixedWidthLineField(Start = 24, Length = 15)]
        public string PurchaseOrderNumber { get; set; }

        [FixedWidthLineField(Start = 39, Length = 15)]
        public string SupplierID { get; set; }

        [FixedWidthLineField(Start = 54, Length = 12)]
        public string OrderDiscount { get; set; }

        [FixedWidthLineField(Start = 66, Length = 8, Format = "ddMMyyyy")]
        public DateTime ReceivedDate { get; set; }

        [FixedWidthLineField(Start = 74, Length = 6)]
        public string ReceivedTime { get; set; }

        [FixedWidthLineField(Start = 80, Length = 15)]
        public string SKUOrUPCCode { get; set; }

        [FixedWidthLineField(Start = 95, Length = 3)]
        public string UOM { get; set; }

        [FixedWidthLineField(Start = 98, Length = 6)]
        public string Quantity { get; set; }

        [FixedWidthLineField(Start = 104, Length = 12)]
        public string CostOfSupplier { get; set; }

        [FixedWidthLineField(Start = 116, Length = 12)]
        public string LineDiscount { get; set; }

        [FixedWidthLineField(Start = 128, Length = 1)]
        public string DeliveryFlag { get; set; }

    }
}

﻿using FlaUI.Core.AutomationElements;
using PDI.Automation.Envoy.Controls;
using PDI.Automation.Envoy.Extensions;
using PDI.Automation.WinForms.Controls;
using PDI.Automation.WinForms.Extensions;
using FlaUI.Core.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlaUI.Core.WindowsAPI;
using FlaUI.Core.Input;
using System.Reflection;
using System.IO;
using FixedWidthParserWriter;
using NUnit.Framework;
using PDI.Automation.Envoy.Database;

namespace PDI.Automation.Envoy.Modules
{
    public class PhysicalCount : EnvoyHome
    {
        private List<Count> CountDetails = new List<Count>();

        public PhysicalCount(Window window, Translations Translations) : base(window, Translations)
        {
        }

        //-----------------"Physical count Batches" window objects-----------------
        Window BatchesWindow => Window.FindFirstDescendantWithRetry(cf => cf.ByName(Translations.Translate("Physical Count Batches"))).AsWindow();

        //Data Grid
        AutomationElement[] BatchesWindowPanes => BatchesWindow.FindAllChildren(cf => cf.ByControlType(FlaUI.Core.Definitions.ControlType.Pane));
        PDIDataGrid BatchesGrid => BatchesWindowPanes[6].AsPDIDataGrid();

        //Buttons
        AutomationElement[] BatchesWindowButtons => BatchesWindow.FindAllDescendants(cf => cf.ByControlType(FlaUI.Core.Definitions.ControlType.Button));
        Button BatchesWindowOKButton => BatchesWindowButtons[0].AsButton();
        Button BatchesRemoveButton => BatchesWindowButtons[1].AsButton();
        Button BatchesNewButton => BatchesWindowButtons[2].AsButton();
        Button BatchesEditButton => BatchesWindowButtons[3].AsButton();
        Button BatchesPrintButton => BatchesWindowButtons[4].AsButton();
        Button BatchesCloseCountButton => BatchesWindowButtons[5].AsButton();


        //-----------------"Physical count Batches Details" window objects-----------------
        Window BatchDetailsWindow => Window.FindFirstDescendantWithRetry(cf => cf.ByAutomationId("_PhysicalCountBatchesCore_List_P_CHeader_PhysicalCountBatchDetails")).AsWindow();

        //Buttons
        AutomationElement[] BatchDetailsWindowButtons => BatchDetailsWindow.FindAllDescendants(cf => cf.ByControlType(FlaUI.Core.Definitions.ControlType.Button));
        Button BatchDetailsWindowQueryButton => BatchDetailsWindowButtons[0].AsButton();
        Button BatchDetailsWindowRemoveMultipleButton => BatchDetailsWindowButtons[1].AsButton();
        Button BatchDetailsWindowNewMultipleButton => BatchDetailsWindowButtons[2].AsButton();
        Button BatchDetailsWindowRemoveButton => BatchDetailsWindowButtons[3].AsButton();
        Button BatchDetailsWindowNewButton => BatchDetailsWindowButtons[4].AsButton();
        Button BatchDetailsWindowOKButton => BatchDetailsWindowButtons[5].AsButton();


        //Validate the data that imported with import file (count.tmp)
        public void Validate(string filePath)
        {
            ReadDataFromInputFile(filePath);

            //var totalRows = BatchesGrid.ListRows("Batch No.");
            //Send the Ctrl + End key to move to the first element
            VirtualKeyShort[] keys = { VirtualKeyShort.CONTROL, VirtualKeyShort.END };
            Keyboard.TypeSimultaneously(keys);

            BatchesEditButton.Click();

            var DetailRows = Grid.ListRows("SKU");

            foreach (var row in DetailRows)
            {
                PhysicalCountBatchValues ActualValue = new PhysicalCountBatchValues();
                int x, y;
                row[0].GetCenterPoint(out x, out y);
                Mouse.MoveTo(x, y);
                Mouse.LeftDoubleClick();

                ProductsWindowEditButton.Click();

                MenuTabItemNavigationPane.GetRequiredTabItemPoint(3, 4, out x, out y);
                Mouse.LeftClick(new System.Drawing.Point(x, y));

                //From Properties Grid get UPC and UOM values
                List<string> ActualUPCList = PropertiesGrid.GetAllColumnValues("UPC/PLU");
                ActualValue.UPC = GetExpectedUPC(ActualUPCList);

                var PropertyCells = PropertiesGrid.GetFirstRowCells();
                //ActualValues.UPC = PropertyCells[2].Value;
                ActualValue.UOM = PropertyCells[1].Value;

                PropertiesWindowOKButton.Click();
                ProductsWindowOKButton.Click();

                //Reading Actual. column value from "PhysicalCountBatchDetails" Window
                string quantityValue = row[4].Value;
                ActualValue.Quantity = quantityValue.Split('.')[0];

                CompareAllValues(GetExpectedCount(ActualValue.UPC), ActualValue);

                Keyboard.Type(VirtualKeyShort.DOWN);
                Wait.UntilInputIsProcessed();
            }

            BatchDetailsWindowOKButton.Click();
            BatchesWindowOKButton.Click();
        }

        public void ClosePhysicalCounts()
        {
            int rowCount = BatchesGrid.GetRowCount("Batch No.");

            for (int iterator = 0; iterator < rowCount; iterator++)
            {
                if(!BatchesCloseCountButton.IsEnabled)
                {
                    Keyboard.Type(VirtualKeyShort.DOWN);
                }
                else
                {
                    BatchesCloseCountButton.Click();
                    ConfirmYesButton.Click();
                }
            }
            BatchesWindowOKButton.Click();
        }

        //To compare expected value and actual value
        private void CompareAllValues(Count ExpectedValue, PhysicalCountBatchValues ActualValue)
        {
            CompareValues("UPCCode", ExpectedValue.UPCCode, ActualValue.UPC);
            CompareValues("UOM", ExpectedValue.UOM, ActualValue.UOM);
            CompareValues("Quantity", ExpectedValue.Quantity, ActualValue.Quantity);
        }


        //Read data from import file
        private void ReadDataFromInputFile(string filePath)
        {
            var dllDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var dataFilePath = dllDir + @"\..\..\Setup\" + filePath;

            List<string> lines = new List<string>();
            using (var sr = new StreamReader(dataFilePath))
            {
                while (sr.Peek() >= 0)
                    lines.Add(sr.ReadLine());
            }

            //Storing the data in Count class object
            CountDetails = new FixedWidthLinesProvider<Count>().Parse(lines);
        }

        //Get Count object with Actual UPC value
        private Count GetExpectedCount(string ActualCountValue)
        {
            var details = CountDetails.Where(x => x.UPCCode == ActualCountValue).ToList().FirstOrDefault();
            return details;
        }


        private string GetExpectedUPC(List<string> upcValue)
        {
            bool flag = false;
            foreach (var item in upcValue)
            {
                var details = CountDetails.Where(x => x.UPCCode == item).ToList().FirstOrDefault();
                if (details != null)
                {
                    flag = true;// false;
                    return item;
                }
            }
            if (!flag)
            {
                throw new Exception("UPC " + upcValue + " value is not present in Unit Of Sale.");
            }
            else
                return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDI.Automation.Envoy.Modules
{
    public class DsdEntryWindowValues
    {
        public string ReceivingDate { get; set; }
        public string DeliveryRefNo { get; set; }
        public string Vendor { get; set; }
        public string OrderDiscountAmt { get; set; }
        public string VendorSKU { get; set; }
        public string UOM { get; set; }
        public string Quantity { get; set; }
        public string UnitCost { get; set; }
        public string LineDiscount { get; set; }
    }
}

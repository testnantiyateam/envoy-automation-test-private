﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDI.Automation.Envoy.Modules
{
    public class PhysicalCountBatchValues
    {

        public string UPC { get; set; }
        public string UOM { get; set; }
        public string Quantity { get; set; }
    }
}

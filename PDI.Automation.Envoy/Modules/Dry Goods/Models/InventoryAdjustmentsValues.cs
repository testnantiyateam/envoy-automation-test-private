﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDI.Automation.Envoy.Modules
{
    public class InventoryAdjustmentsValues
    {
        public string UPC { get; set; }
        public string Quantity { get; set; }
        public string AdjustmentType { get; set; }
        public string UOM { get; set; }
    }
}

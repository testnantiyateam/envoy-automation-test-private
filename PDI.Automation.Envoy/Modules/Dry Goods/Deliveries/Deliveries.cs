﻿using FixedWidthParserWriter;
using FlaUI.Core.AutomationElements;
using FlaUI.Core.Input;
using FlaUI.Core.WindowsAPI;
using PDI.Automation.Envoy.Database;
using PDI.Automation.Envoy.Extensions;
using PDI.Automation.WinForms.Controls;
using PDI.Automation.WinForms.Extensions;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace PDI.Automation.Envoy.Modules
{
    public class Deliveries : EnvoyHome
    {
        private List<Dsd> invoiceDetails = new List<Dsd>();

        public Deliveries(Window window, Translations Translations) : base(window, Translations)
        {

        }

        //Store Deliveries Objects
        Window StoreDeliveriesWindow => Window.FindFirstDescendantWithRetry(cf => cf.ByAutomationId("_DSDFrontScreenCore_Tab_StoreDeliveries")).AsWindow();
        Pane StoreDeliveriesOKPane => StoreDeliveriesWindow.FindFirstDescendantWithRetry(cf => cf.ByAutomationId("btnB_OK")).AsPane();
        Button StoreDeliveriesOKButton => StoreDeliveriesOKPane.FindFirstDescendant(cf => cf.ByControlType(FlaUI.Core.Definitions.ControlType.Button)).AsButton();


        //-----------------DSD Entry Objects-----------------
        Window DSDEntryStoreWindow => Window.FindFirstDescendantWithRetry(cf => cf.ByAutomationId("DSDDetailsCore_DSDEntry_DSD")).AsWindow();
        AutomationElement[] TextBoxPanes => DSDEntryStoreWindow.FindAllDescendants(cf => cf.ByAutomationId("TextBox").And(cf.ByControlType(FlaUI.Core.Definitions.ControlType.Pane)));

        TextBox DeliveryRefNo => TextBoxPanes[13].FindAllChildren().FirstOrDefault().AsTextBox();

        //OK button
        Pane OKButtonPane => DSDEntryStoreWindow.FindFirstDescendant(cf => cf.ByAutomationId("btnB_OK").And(cf.ByControlType(FlaUI.Core.Definitions.ControlType.Pane))).AsPane();
        Button DsdEntryOKButton => OKButtonPane.FindFirstDescendant(cf => cf.ByControlType(FlaUI.Core.Definitions.ControlType.Button)).AsButton();


        Pane AmountToPayPane => TextBoxPanes[1].AsPane();
        Pane OrderDiscountPane => TextBoxPanes[2].AsPane();
        Pane TotalAfterAllDiscountPane => TextBoxPanes[3].AsPane();
        Pane TaxesPane => TextBoxPanes[4].AsPane();
        Pane NetAmountPane => TextBoxPanes[5].AsPane();
        Pane OrderDiscountAmtPane => TextBoxPanes[15].AsPane();
        Pane TotalAfterOrderDiscountAmtPane => TextBoxPanes[7].AsPane();
        Pane ReceivingDatePane => TextBoxPanes[21].AsPane();
        Pane VendorNamePane => TextBoxPanes[9].AsPane();
        Pane ReceiverPane => TextBoxPanes[10].AsPane();
        Pane VendorPane => TextBoxPanes[24].AsPane();

        Pane LineDiscountPane => TextBoxPanes[3].AsPane();

        Button DsdEntryOkButton => DSDEntryStoreWindow.FindFirstDescendant(cf => cf.ByAutomationId("btnB_OK")).AsButton();

        //Validate the data that imported with import file (dsd.tmp)
        public void Validate(string filePath)
        {
            //Read the data from the filepath
            ReadDataFromInputFile(filePath);

            var totalRows = Grid.ListRows("Delivery No.");
            
            //Iterate the each table row to validate
            foreach (var row in totalRows)
            {
                //To open DSD Entry window, clicking Enter key
                Keyboard.Type(VirtualKeyShort.ENTER);

                //Get imported values from the Envoy forms.
                DsdEntryWindowValues ActualData = GetDsdEntryValues();
                
                //Get imported values from import file.
                Dsd ExpectedData = GetInvoiceDetails(ActualData.DeliveryRefNo);

                //Comparing the values
                CompareAllValues(ExpectedData, ActualData);

                DsdEntryOKButton.Click();
                Keyboard.Type(VirtualKeyShort.DOWN);
            }
            StoreDeliveriesOKButton.Click();
        }

        //Read input data file
        private void ReadDataFromInputFile(string filePath)
        {
            var dllDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var dataFilePath = dllDir + @"\..\..\Setup\" + filePath;

            List<string> lines = new List<string>();
            using (var sr = new StreamReader(dataFilePath))
            {
                while (sr.Peek() >= 0)
                    lines.Add(sr.ReadLine());
            }

            //Storing the data in Dsd class object
            invoiceDetails = new FixedWidthLinesProvider<Dsd>().Parse(lines);
        }

        private Dsd GetInvoiceDetails(string invoiceNumber)
        {
            var details = invoiceDetails.Where(x => x.InvoiceNumber == invoiceNumber).ToList().FirstOrDefault();
            return details;
        }

        //Compare the values
        private void CompareAllValues(Dsd ExpectedValue, DsdEntryWindowValues ActualValue)
        {
            //To compare all the required values from Dsd.temp file
            CompareValues("InvoiceDate", ExpectedValue.InvoiceDate.ToString("dd MMM yyyy"), ActualValue.ReceivingDate);
            CompareValues("InvoiceNumber", ExpectedValue.InvoiceNumber, ActualValue.DeliveryRefNo);
            CompareValues("Vendor", ExpectedValue.SupplierID, ActualValue.Vendor);

            //Making Actual OrderDiscountAmt value to decimal followed by 4 zeros if value is empty
            if (string.IsNullOrEmpty(ActualValue.OrderDiscountAmt))
            {
                ActualValue.OrderDiscountAmt = "0.0000";
            }
            CompareValues("OrderDiscount", ExpectedValue.OrderDiscount, ActualValue.OrderDiscountAmt);
            CompareValues("SKUOrUPCCode", ExpectedValue.SKUOrUPCCode, ActualValue.VendorSKU);
            CompareValues("UOM", ExpectedValue.UOM, ActualValue.UOM);

            //Truncating Actual Quantity value
             if(!ExpectedValue.Quantity.Contains("."))
                ActualValue.Quantity = ActualValue.Quantity.Split('.')[0];
            CompareValues("Quantity", ExpectedValue.Quantity, ActualValue.Quantity);
            CompareValues("CostOfSupplier", ExpectedValue.CostOfSupplier, ActualValue.UnitCost);
        }

        //Store the actual values into DsdEntryWindowValues object
        private DsdEntryWindowValues GetDsdEntryValues()
        {
            DsdEntryWindowValues values = new DsdEntryWindowValues();
            values.DeliveryRefNo = DeliveryRefNo.Text;
            values.ReceivingDate = ReceivingDatePane.GetWindowText();
            values.Vendor = VendorPane.GetWindowText();
            values.OrderDiscountAmt = OrderDiscountAmtPane.GetWindowText();
            values.LineDiscount = LineDiscountPane.GetWindowText();

            Grid.Click();
            var itemLevelRows = Grid.ListRows(Translations.Translate("Vendor SKU"));

            values.VendorSKU = itemLevelRows.FirstOrDefault()[2].Value;
            values.UOM = itemLevelRows.FirstOrDefault()[4].Value;
            values.Quantity = itemLevelRows.FirstOrDefault()[5].Value;
            values.UnitCost = itemLevelRows.FirstOrDefault()[6].Value;

            return values;
        }

    }
}

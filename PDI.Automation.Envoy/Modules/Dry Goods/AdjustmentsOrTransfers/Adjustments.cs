﻿using FixedWidthParserWriter;
using FlaUI.Core.AutomationElements;
using FlaUI.Core.Input;
using FlaUI.Core.WindowsAPI;
using PDI.Automation.Envoy.Controls;
using PDI.Automation.Envoy.Database;
using PDI.Automation.Envoy.Extensions;
using PDI.Automation.WinForms.Controls;
using PDI.Automation.WinForms.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace PDI.Automation.Envoy.Modules
{
    public class Adjustments : EnvoyHome
    {
        private List<Adj> adjDetails = new List<Adj>();

        private string adjDate = string.Empty;

        private string todaysDate = DateTime.Now.ToString("dd MMM yyyy");

        public Adjustments(Window window, Translations Translations) : base(window, Translations)
        {

        }

        //Inventory Adjustments Window Objects
        Window InventoryAdjustmentsWindow => Window.FindFirstDescendantWithRetry(cf => cf.ByName(Translations.Translate("Inventory Adjustments")),TimeSpan.FromSeconds(2))?.AsWindow();
        AutomationElement [] InventoryAdjustmentsPanes => InventoryAdjustmentsWindow.FindAllChildren(cf => cf.ByControlType(FlaUI.Core.Definitions.ControlType.Pane));
        PDIDataGrid InventoryAdjustmentsGrid => InventoryAdjustmentsPanes[5].AsPDIDataGrid();

        AutomationElement[] BatchesWindowButtons => InventoryAdjustmentsWindow.FindAllDescendants(cf => cf.ByControlType(FlaUI.Core.Definitions.ControlType.Button));

        Button InventoryAdjustmentsWindowOKButton => BatchesWindowButtons[0].AsButton();
        Button InventoryAdjustmentsWindowRemoveButton => BatchesWindowButtons[1].AsButton();
        Button InventoryAdjustmentsWindowNewButton => BatchesWindowButtons[2].AsButton();
        Button InventoryAdjustmentsWindowEditButton => BatchesWindowButtons[3].AsButton();
        Button InventoryAdjustmentsWindowPrintButton => BatchesWindowButtons[4].AsButton();

        //-----------------Inventory Adjustments Details Window Objects-----------------
        Window InventoryAdjustmentsDetailsWindow => Window.FindFirstDescendantWithRetry(cf => cf.ByAutomationId("_InvAdjustmentCore_InventoryAdjustments_task_AdjustmentHeader_InventoryAdjustments"),TimeSpan.FromSeconds(2))?.AsWindow();
        Button InventoryAdjustmentsDetailsWindowOKButton => InventoryAdjustmentsWindow.FindAllDescendants(cf => cf.ByControlType(FlaUI.Core.Definitions.ControlType.Button))[0].AsButton();

        public void Validate(string filePath)
        {
            ReadDataFromInputFile(filePath);
            
            //Select the row that is of today's date
            InventoryAdjustmentsGrid.SelectRow("Date", todaysDate);
            InventoryAdjustmentsWindowEditButton.Click();

            var DetailRows = Grid.ListRows("SKU");

            //Iterate through each value
            foreach (var row in DetailRows)
            {
                InventoryAdjustmentsValues ActualValue = new InventoryAdjustmentsValues();
                int x, y;
                row[0].GetCenterPoint(out x, out y);
                Mouse.MoveTo(x, y);
                Mouse.LeftDoubleClick();

                ProductsWindowEditButton.Click();

                //Navigate to "Unit Of sale" menu item
                MenuTabItemNavigationPane.GetRequiredTabItemPoint(3, 4, out x, out y);
                Mouse.LeftClick(new System.Drawing.Point(x, y));

                //Below statement gets all UPC values from all rows.
                List<string> ActualUPCList = PropertiesGrid.GetAllColumnValues("UPC/PLU");
                ActualValue.UPC = GetExpectedUPC(ActualUPCList);

                PropertiesWindowOKButton.Click();
                ProductsWindowOKButton.Click();

                //Reading UOM, AdjustmentType and Quantity values from "Inventory Adjustments" Window
                ActualValue.UOM = row[2].Value;
                ActualValue.AdjustmentType = row[3].Value;
                string quantityValue = row[4].Value;
                ActualValue.Quantity = quantityValue.Split('.')[0];

                CompareAllValues(GetExpectedAdjustment(ActualValue.UPC), ActualValue);

                Keyboard.Type(VirtualKeyShort.DOWN);
                Wait.UntilInputIsProcessed();
            }

            InventoryAdjustmentsDetailsWindowOKButton.Click();
            InventoryAdjustmentsWindowOKButton.Click();
        }

        //Remove Existing Inventory adjustments that are in today's date.
        public void RemoveTodaysInventories()
        {
            if (InventoryAdjustmentsDetailsWindow != null)
            {
                InventoryAdjustmentsDetailsWindow.Close();
                return; //If InventoryAdjustmentsDetailsWindow gets opened, then no values exist to remove, hence returning the mehtod.
            }

            var totalRows = InventoryAdjustmentsGrid.ListRows("Date");

            foreach (var row in totalRows)
            {
                if(row[2].Value.Equals(todaysDate))
                {
                    InventoryAdjustmentsWindowRemoveButton.Click();
                    ConfirmYesButton.Click();
                }
                Keyboard.Type(VirtualKeyShort.DOWN);
            }

            //Checks if InventoryAdjustmentsWindow still exists, if it exists, closes it.
            if (InventoryAdjustmentsWindow != null)
                InventoryAdjustmentsWindow.Close();
        }

        private void ReadDataFromInputFile(string filePath)
        {
            var dllDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var dataFilePath = dllDir + @"\..\..\Setup\" + filePath;

            List<string> lines = new List<string>();
            using (var sr = new StreamReader(dataFilePath))
            {
                while (sr.Peek() >= 0)
                    lines.Add(sr.ReadLine());
            }

            //Storing the data in Adj class object
            adjDetails = new FixedWidthLinesProvider<Adj>().Parse(lines);
        }

        //Get Adj object with Actual UPC value
        private Adj GetExpectedAdjustment(string ActualAdjustmentValue)
        {
            var details = adjDetails.Where(x => x.UPCCode == ActualAdjustmentValue).ToList().FirstOrDefault();
            return details;
        }

        //Compare string values.
        private void CompareAllValues(Adj ExpectedValue, InventoryAdjustmentsValues ActualValue)
        {
            CompareValues("UPCCode", ExpectedValue.UPCCode, ActualValue.UPC);
            CompareValues("UOM", ExpectedValue.UOM, ActualValue.UOM);
            CompareValues("AdjustmentType", ExpectedValue.AdjustmentType, ActualValue.AdjustmentType);
            CompareValues("Quantity", ExpectedValue.Quantity, ActualValue.Quantity);
        }
        

        //From all UPC values lists, checking which UPC value is expected to be matching. If no upc value is matching, failing the test case.
        private string GetExpectedUPC(List<string> upcValues)
        {
            string requiredUPC = string.Empty;
            foreach (var item in upcValues)
            {
                var details = adjDetails.Where(x => x.UPCCode == item).ToList().FirstOrDefault();
                if (details != null)
                {
                    requiredUPC = item;
                    break;
                }
            }
            if (string.IsNullOrEmpty(requiredUPC))
            {
                throw new Exception("No UPC value from Unit Of Sale window is matching with any of the expected UPC codes.");
            }
            else
                return requiredUPC;
        }
    }
}

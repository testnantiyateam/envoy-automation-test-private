﻿using FlaUI.Core.AutomationElements;
using FlaUI.Core.Input;
using FlaUI.Core.Tools;
using FlaUI.Core.WindowsAPI;
using PDI.Automation.Envoy.Controls;
using PDI.Automation.Envoy.Extensions;
using PDI.Automation.WinForms.Controls;
using PDI.Automation.WinForms.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlaUI.Core.Definitions;
using NUnit.Framework;
using PDI.Automation.Envoy.Database;

namespace PDI.Automation.Envoy.Modules
{
    public class EnvoyHome : Window
    {
        protected Window Window { get; set; }
        protected Translations Translations { get; set; }

        public EnvoyHome(Window window, Translations translations) : base(window.FrameworkAutomationElement)
        {
            Window = window;
            Translations = translations;
        }

        private AutomationElement [] MenuBars => Window.FindAllDescendants(cf => cf.ByControlType(ControlType.MenuBar));
        private AutomationElement OptionsMenu => MenuBars[0].FindAllChildren(cf => cf.ByControlType(ControlType.MenuItem))[2];
        private Window SwitchLanguageWindow => Window.FindFirstDescendantWithRetry(cf => cf.ByAutomationId("2")).AsWindow();
        private PDIDataGrid SwitchLanguageGrid => SwitchLanguageWindow.FindAllChildren(cf => cf.ByControlType(ControlType.Pane))[5].AsPDIDataGrid();
        private Button SwitchLanguageOkButton => SwitchLanguageWindow.FindAllDescendants(cf => cf.ByControlType(ControlType.Button))[1].AsButton();

        private Window SwitchLanguagePopUpWindow => Window.FindFirstDescendantWithRetry(cf => cf.ByControlType(ControlType.Window)).AsWindow();
        private Button SwitchLanguageConfirmOkButton => SwitchLanguagePopUpWindow.FindAllDescendants(cf => cf.ByControlType(ControlType.Button))[1].AsButton();
        private Button SwitchLanguageAppliedOkButton => SwitchLanguagePopUpWindow.FindAllDescendants(cf => cf.ByControlType(ControlType.Button))[0].AsButton();

        protected Button MaximizeButton => Window.FindFirstDescendant(cf => cf.ByName("Maximize")).AsButton();
        protected Toolbar Toolbar => Window.FindFirstDescendant(cf => cf.ByAutomationId("MainMenu")).AsToolbar();
        protected Button OKButton => Window.FindFirstDescendantWithRetry(cf => cf.ByName(Translations.Translate("OK"))).AsButton();
        protected Button CancelButton => Window.FindFirstDescendantWithRetry(cf => cf.ByName("Cancel")).AsButton();
        protected Button ConfirmYesButton => Window.FindFirstDescendantWithRetry(cf => cf.ByName(Translations.Translate("Yes"))).AsButton();
        protected Button ConfirmNoButton => Window.FindFirstDescendantWithRetry(cf => cf.ByName(Translations.Translate("No"))).AsButton();

        protected PDIDataGrid Grid => Window.FindFirstDescendantWithRetry(cf => cf.ByAutomationId("grd")).AsPDIDataGrid();

        //-----------------"Products" window objects-----------------
        protected Window ProductsWindow => Window.FindFirstDescendantWithRetry(cf => cf.ByAutomationId("ProductCore_Products_DefaultScreen_EXP")).AsWindow();

        //Buttons
        protected AutomationElement[] ProductsWindowButtons => ProductsWindow.FindAllDescendants(cf => cf.ByControlType(ControlType.Button));
        protected Button ProductsWindowCancelButton => ProductsWindowButtons[0].AsButton();
        protected Button ProductsWindowOKButton => ProductsWindowButtons[1].AsButton();
        protected Button ProductsWindowRemoveButton => ProductsWindowButtons[2].AsButton();
        protected Button ProductsWindowNewButton => ProductsWindowButtons[3].AsButton();
        protected Button ProductsWindowEditButton => ProductsWindowButtons[4].AsButton();
        protected Button ProductsWindowDeactivateButton => ProductsWindowButtons[5].AsButton();
        protected Button ProductsWindowSetupRulesButton => ProductsWindowButtons[6].AsButton();
        protected Button ProductsWindowCustomFieldsButton => ProductsWindowButtons[7].AsButton();
        protected Button ProductsWindowSearchButton => ProductsWindowButtons[8].AsButton();


        //-----------------"Properties of Product" window objects-----------------
        protected Window PropertiesWindow => Window.FindFirstDescendantWithRetry(cf => cf.ByAutomationId("e_ProductProperties_EXP")).AsWindow();
        protected Pane MenuTabItemNavigationPane => Window.FindFirstDescendantWithRetry(cf => cf.ByAutomationId("tabVTabSelection_ONSCREEN")).AsPane();

        //To elements were found with grd id, hence taking the last element.
        protected PDIDataGrid PropertiesGrid => PropertiesWindow.FindAllDescendantsWithRetry(cf => cf.ByAutomationId("grd")).Last().AsPDIDataGrid();

        //Buttons
        protected AutomationElement[] PropertiesWindowButtons => PropertiesWindow.FindAllDescendants(cf => cf.ByControlType(ControlType.Button));

        protected Button PropertiesWindowOKButton => PropertiesWindowButtons[3].AsButton();
        protected Button PropertiesWindowCancelButton => PropertiesWindowButtons[4].AsButton();
        protected Button PropertiesWindowCustomFieldsEntryButton => PropertiesWindowButtons[5].AsButton();
        protected Button PropertiesWindowAssociatedFeesButton => PropertiesWindowButtons[6].AsButton();
        protected Button PropertiesWindowTanksButton => PropertiesWindowButtons[7].AsButton();
        protected Button PropertiesWindowUsedByButton => PropertiesWindowButtons[8].AsButton();
        protected Button PropertiesWindowOnOrderButton => PropertiesWindowButtons[9].AsButton();
        protected Button PropertiesWindowOtherDetailsButton => PropertiesWindowButtons[10].AsButton();
        protected Button PropertiesWindowRangeDropDownButton => PropertiesWindowButtons[11].AsButton();

        public void OpenMenu(string menu)
        {
            string [] navigationMenus = menu.Split('-');
            navigationMenus = GetTranslatedValues(navigationMenus);
            MenuItem parentMenu = Window.FindFirstDescendantWithRetry(cf => cf.ByName(navigationMenus[0])).AsMenuItem();
            ClickChildMenu(parentMenu, navigationMenus);
        }

        public void ClickOK()
        {
            OKButton.Click();
        }
        public void ClickCancel()
        {
            CancelButton.Click();
        }

        private void ClickChildMenu(MenuItem parentMenuItem, string [] navigationMenus)
        {
            parentMenuItem.Click();
            Keyboard.Type(VirtualKeyShort.DOWN);
            var childMenus = navigationMenus.Skip(1);
            foreach (var item in childMenus)
            {
                var children = parentMenuItem.Items;
                var child = children[item];
                if (child == null)
                    throw new Exception(string.Format("Could not find the menu item \"{0}\". Check that correct spelling is given for navigation", item));

                var position = children.IndexOf(child);

                for (int iterator = 1; iterator <= position; iterator++)
                {
                    Keyboard.Type(VirtualKeyShort.DOWN);
                }

                //If it has children, expand the menu
                if (child.Items.Count > 0)
                    Keyboard.Type(VirtualKeyShort.RIGHT);

                parentMenuItem = child;
            }
            Keyboard.Type(VirtualKeyShort.ENTER);
        }

        //Compare the Expected value and Actual value. If there is any mismatch, test case will be failed.
        public void CompareValues(string fieldName, string expected, string actual)
        {
            //To compare the value. Test case will get failed if mismatch in values.
            Assert.AreEqual(expected, actual.Trim(), "Values are not matching for the field \"{0}\".\nExpected Value : {1} ; Actual Value: {2}", fieldName, expected, actual);
        }

        //To close all winforms that were opened
        public void CloseAllExistingOpenedMenus()
        {
            var pane = Window.FindFirstChild(x => x.ByControlType(ControlType.Pane));
            var openedWindows = pane.FindAllChildren(x => x.ByControlType(ControlType.Window));
            foreach (var window in openedWindows)
            {
                window.AsWindow().Close();
                Wait.UntilInputIsProcessed();
            }
        }

        //ToSwitch Language to other languages.
        public void SwitchLanguage(string Language)
        {
            OptionsMenu.Click();
            Keyboard.Type(VirtualKeyShort.UP);
            Keyboard.Type(VirtualKeyShort.ENTER);

            if(SwitchLanguageGrid.GetSelectedValue("Language Code").Equals(Language,StringComparison.OrdinalIgnoreCase))
            {
                SwitchLanguageWindow.Close();
                return;
            }
            SwitchLanguageGrid.SelectRow("Language Code", Language);
            SwitchLanguageOkButton.Click();
            SwitchLanguageConfirmOkButton.Click();
            SwitchLanguageAppliedOkButton.Click();
        }

        private string[] GetTranslatedValues(string[] arrValues)
        {
            return arrValues.Select(x => Translations.Translate(x)).ToArray();
        }
    }
}

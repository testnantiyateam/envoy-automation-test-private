﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using BoDi;
using FlaUI.Core;
using FlaUI.Core.Tools;
using FlaUI.UIA3;
using PDI.Automation.Envoy.Tests.Configuration;
using TechTalk.SpecFlow;
using FlaUI.Core.AutomationElements;
using PDI.Automation.Envoy.Modules;
using System.Drawing;
using FlaUI.Core.Input;

[Binding]
public class EnvoyHooks
{

    //Set the object container to register objects to be injected
    private static IObjectContainer _container;
    //Application the test is running against
    private static Application _app;
    //Automation method to use
    private static AutomationBase _automation;
    private static EnvironmentConfig _config;
    private static Window window;

    //Constructor for hooks that accepts a container
    public EnvoyHooks(IObjectContainer objectContainer)
    {
        _container = objectContainer;
    }

    [BeforeFeature("Envoy")]
    public static void LaunchEnvoy()
    {
        //Store all the EnvoyTestSettings configuration values into _config object.
        _config = new EnvironmentConfig();

        //Set the default timeout for retries
        Retry.Timeout = TimeSpan.FromSeconds(30);

        _app = Application.AttachOrLaunch(new ProcessStartInfo(Path.Combine(_config.EnvoyDirectory.FullName, "Envoy.exe")));
        _automation = new UIA3Automation();

        //Set the COM timeout to more than the default 2 seconds
        _automation.ConnectionTimeout = TimeSpan.FromSeconds(10);

        Retry.WhileNull(() =>
        {
            window = _app.GetMainWindow(_automation);
            return window;
        }, TimeSpan.FromSeconds(30));

        window.SetForeground();

        TextBox userID = window.FindFirstDescendant(cf => cf.ByAutomationId("txtUserId")).AsTextBox();
        if (userID != null)
        {
            userID.Enter(_config.Username);

            Button okButton = window.FindFirstDescendant(cf => cf.ByAutomationId("btnOK")).AsButton();
            okButton.Click();
            System.Threading.Thread.Sleep(5000);
        }

        Retry.WhileNull(() =>
        {
            window = _app.GetMainWindow(_automation);
            return window;
        }, TimeSpan.FromSeconds(30));

        //If application is in minimized state, click on desktop taskbar to open the application
        if (!window.TryGetClickablePoint(out Point point))
        {
            window.Automation.GetDesktop().FindFirstDescendant(x => x.ByName("Envoy - 1 running window")).AsButton().Click();
            Wait.UntilInputIsProcessed();
        }

        //Maximize the Envoy application if window is restored.
        Button MaximizeButton = window.TitleBar.FindFirstDescendant(x => x.ByName("Maximize"))?.AsButton();
        if (MaximizeButton != null)
        {
            MaximizeButton.Click();
        }

        //Switch the Envoy application as per the configured language.
        EnvoyHome envoyHomeObj = new EnvoyHome(window, _config.Translations);
        envoyHomeObj.SwitchLanguage(_config.Language);
    }

    [BeforeScenario]
    public void SetCommonVariables()
    {
        EnvoyHome envoyHomeObj = new EnvoyHome(window, _config.Translations);
        envoyHomeObj.CloseAllExistingOpenedMenus();

        var context = _container.Resolve<ScenarioContext>();
        context.Add("CurrentDate", DateTime.Today);
        context.Add("CurrentPeriod", DateTime.Today.ToString("MM/yyyy"));
    }

    [BeforeScenario("Envoy")]
    public void RegisterWindow()
    {
        var window = _app.GetMainWindow(_automation);
        window.SetForeground();

        _container.RegisterInstanceAs(window);
        _container.RegisterInstanceAs(_config);
    }


    [AfterFeature("Envoy")]
    public static void CloseEnvoy()
    {
        _app.Close();
        _app.Dispose();
        _automation.Dispose();
    }
}
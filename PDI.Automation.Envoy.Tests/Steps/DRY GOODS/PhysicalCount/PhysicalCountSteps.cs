﻿using BoDi;
using PDI.Automation.Envoy.Modules;
using System;
using System.IO;
using TechTalk.SpecFlow;

namespace PDI.Automation.Envoy.Tests.Steps
{
    [Binding]
    class PhysicalCountSteps : EnvoyBaseSteps
    {
        private readonly PhysicalCount physicalCount;

        public PhysicalCountSteps(IObjectContainer objectContainer) : base(objectContainer)
        {
            physicalCount = new PhysicalCount(Window, Config.Translations);
        }

        [Then(@"Validate imported Physical Count Batches with (.*) file")]
        public void ValidateImportedPhysicalCountBatcheswithWithFile(string filePath)
        {
            physicalCount.Validate(filePath);
        }

        [Given(@"Close all the counts from Physical Count Batches")]
        public void GivenCloseAllTheCountsFromPhysicalCountBatches()
        {
            physicalCount.ClosePhysicalCounts();
        }
    }
}

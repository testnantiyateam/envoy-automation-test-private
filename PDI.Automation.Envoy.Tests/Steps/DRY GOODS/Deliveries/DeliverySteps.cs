﻿using BoDi;
using PDI.Automation.Envoy.Modules;
using System;
using System.IO;
using TechTalk.SpecFlow;

namespace PDI.Automation.Envoy.Tests.Steps
{
    [Binding]
    class DeliverySteps : EnvoyBaseSteps
    {
        private readonly Deliveries deliveries;

        public DeliverySteps(IObjectContainer objectContainer) : base(objectContainer)
        {
            deliveries = new Deliveries(Window, Config.Translations);
        }

        [Then(@"Validate imported DSD on Store deliveries with (.*) file")]
        public void ThenValidateImportedDSDOnStoreDeliveriesWithFile(string filePath)
        {
            deliveries.Validate(filePath);
        }
    }
}

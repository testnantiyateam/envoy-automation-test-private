﻿using BoDi;
using PDI.Automation.Envoy.Modules;
using System;
using System.IO;
using TechTalk.SpecFlow;

namespace PDI.Automation.Envoy.Tests.Steps
{
    [Binding]
    class AdjustmentSteps : EnvoyBaseSteps
    {
        private readonly Adjustments adjustments;

        public AdjustmentSteps(IObjectContainer objectContainer) : base(objectContainer)
        {
            adjustments = new Adjustments(Window, Config.Translations);
        }

        [Then(@"Validate imported Inventory Adjustments with (.*) file")]
        public void ThenValidateImportedInventoryAdjustmentsWithFile(string filePath)
        {
            adjustments.Validate(filePath);
        }

        [Given(@"Remove existing Inventory Adjustments that is of today's date")]
        public void GivenRemoveExistingInventoryAdjustmentsThatIsOfTodaySDate()
        {
            adjustments.RemoveTodaysInventories();
        }
    }
}

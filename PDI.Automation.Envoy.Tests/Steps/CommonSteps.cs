﻿using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using PDI.Automation.Envoy.Modules;
using FlaUI.Core.AutomationElements;
using PDI.Automation.Envoy.Tests.Configuration;
using BoDi;

namespace PDI.Automation.Envoy.Tests.Steps
{
    [Binding]
    public class CommonSteps : EnvoyBaseSteps
    {
        private readonly EnvoyHome envoyHome;

        public CommonSteps(IObjectContainer objectContainer) : base(objectContainer)
        {
            envoyHome = new EnvoyHome(Window, Config.Translations);
        }

        [Then(@"I navigate to (.*)")]
        [Given(@"I have navigated to (.*)")]
        public void GivenIHaveNavigatedTo(string menu)
        {
            envoyHome.OpenMenu(menu);
        }

    }
}

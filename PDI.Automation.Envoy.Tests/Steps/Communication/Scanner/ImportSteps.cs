﻿using BoDi;
using PDI.Automation.Envoy.Modules;
using System.IO;
using System.Linq;
using System.Reflection;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace PDI.Automation.Envoy.Tests.Steps.Communication.Scanner
{
    [Binding]
    public class ImportSteps : EnvoyBaseSteps
    {
        private readonly ImportFromScanner importFromScanner;

        public ImportSteps(IObjectContainer objectContainer) : base(objectContainer)
        {
            importFromScanner = new ImportFromScanner(Window, Config.Translations);
        }


        [Given(@"I stage import file (.*)")]
        public void GivenIStageImportFile(string filePath)
        {
            //Copy the required file into the directory "C:\EnvoyCoFolders\PDI\Scanner\Import"
            string[] paths = filePath.Split('\\');
            string fileName = paths[paths.Count() - 1];
            var inputDir = Config.InputDirectory;

            var dllDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var sourceFilePath = dllDir + @"\..\..\Setup\" + filePath;

            File.Copy(sourceFilePath, inputDir + "\\" + fileName, true);

        }

        [When(@"I Enter a import file preparation date for")]
        public void GivenIEnterAImportFilePreparationDateFor(ImportFilePreparation date)
        {
            importFromScanner.Import(date);
        }

        [Then(@"Verify the successfull imported from scanner")]
        public void ThenVerifyTheSuccessfullImportedFromScanner()
        {
            importFromScanner.AcknowledgeMessage();
        }

        [StepArgumentTransformation]
        public ImportFilePreparation TableToPaymentEntry(Table table)
        {
            ReplaceTableVariables(table);
            return table.CreateInstance<ImportFilePreparation>();
        }
    }
}

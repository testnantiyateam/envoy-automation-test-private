﻿using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using FlaUI.Core.AutomationElements;
using PDI.Automation.Envoy.Tests.Configuration;
using BoDi;

namespace PDI.Automation.Envoy.Tests.Steps
{
    public abstract class EnvoyBaseSteps : TechTalk.SpecFlow.Steps
    {
        /// <summary>
        /// Represents the Injected ObjectContainer.
        /// </summary>
        private readonly IObjectContainer objectContainer;

        public EnvoyBaseSteps(IObjectContainer objectContainer )
        {
            this.objectContainer = objectContainer;
        }

        protected ScenarioContext Context => objectContainer.Resolve<ScenarioContext>();
        protected EnvironmentConfig Config => objectContainer.Resolve<EnvironmentConfig>();
        protected Window Window => objectContainer.Resolve<Window>();

        protected List<string> Variables = new List<string>() { "CurrentDate", "CurrentPeriod" };

        [StepArgumentTransformation]
        protected string ReplaceStringVariables(string input)
        {
            return ReplaceVariable(input);
        }

        [StepArgumentTransformation]
        protected Table ReplaceTableVariables(Table input)
        {
            foreach (TableRow row in input.Rows)
            {
                foreach (var key in row.Keys)
                {
                    var value = row[key];
                    row[key] = ReplaceVariable(value);
                }
            }
            return input;
        }

        private string ReplaceVariable(string input, int offset = 0)
        {
            if (Variables.Contains(input))
            {
                Context.TryGetValue(input, out var variableValue);
                input = variableValue.ToString();
            }
            else
            {
                var variableChar = '`';

                var startIndex = input.IndexOf(variableChar, offset);

                if (startIndex >= 0)
                {
                    var endIndex = input.IndexOf(variableChar, startIndex + 1);
                    if (endIndex > startIndex)
                    {
                        var variableName = input.Substring(startIndex + 1, endIndex - startIndex - 1);
                        Context.TryGetValue(variableName, out var variableValue);

                        input = input.Substring(0, startIndex) + variableValue + input.Substring(endIndex + 1);

                        if (input.Length > endIndex + 1)
                            input = ReplaceVariable(input, endIndex + 1);
                    }
                }
            }
            return input;
        }

    }
}
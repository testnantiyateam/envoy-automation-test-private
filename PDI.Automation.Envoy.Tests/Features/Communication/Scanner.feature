﻿@Envoy
Feature: Scanner

Scenario: Import DSD from scanner
	Given I stage import file Scanner\Import\dsd.tmp
	And I have navigated to COMMUNICATION-Scanner-Import from Scanner
	When I Enter a import file preparation date for
	| adjustmentdate | inventorycountdate |
	| CurrentDate    | CurrentDate        |
	Then Verify the successfull imported from scanner
	And I navigate to DRY GOODS-Deliveries-Deliveries
	And Validate imported DSD on Store deliveries with Scanner\Import\dsd.tmp file

Scenario: Import Physical count from scanner
	Given I have navigated to DRY GOODS-Physical Count-Physical Count
	And Close all the counts from Physical Count Batches
	And I stage import file Scanner\Import\count.tmp
	And I have navigated to COMMUNICATION-Scanner-Import from Scanner
	When I Enter a import file preparation date for
	| adjustmentdate | inventorycountdate |
	| CurrentDate    | CurrentDate        |
	Then Verify the successfull imported from scanner
	And I navigate to DRY GOODS-Physical Count-Physical Count
	And Validate imported Physical Count Batches with Scanner\Import\count.tmp file

@CopyInputFiles
Scenario: Import Adjustment from Scanner
	Given I have navigated to DRY GOODS-Adjustment/Transfers-Adjustments
	And Remove existing Inventory Adjustments that is of today's date
	And I stage import file Scanner\Import\adj.tmp
	And I have navigated to COMMUNICATION-Scanner-Import from Scanner
	When I Enter a import file preparation date for
	| adjustmentdate | inventorycountdate |
	| CurrentDate    | CurrentDate        |
	Then Verify the successfull imported from scanner
	And I navigate to DRY GOODS-Adjustment/Transfers-Adjustments
	And Validate imported Inventory Adjustments with Scanner\Import\adj.tmp file
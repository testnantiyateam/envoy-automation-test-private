﻿using Dapper;
using Microsoft.Extensions.Configuration;
using PDI.Automation.Envoy.Database;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDI.Automation.Envoy.Tests.Configuration
{
    public class EnvironmentConfig
    {
        public string Language { get; set; }
        public DirectoryInfo EnvoyDirectory { get; set; }
        public string SQLServer { get; set; }
        public string MasterDBName { get; set; }
        public string DataDBName { get; set; }
        public string EnctlDBName { get; set; }
        public string DBUser { get; set; }
        public string DBPassword { get; set; }
        public DirectoryInfo InputDirectory { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DatabaseContext DBContext { get; set; }
        public Translations Translations { get; set; }

        public EnvironmentConfig()
        {
            var config = new ConfigurationBuilder().AddUserSecrets("EnvoyTestConfiguration").Build();

            Language = config["Language"] ?? Environment.GetEnvironmentVariable("Language");
            EnvoyDirectory = new DirectoryInfo(config["EnvoyDirectory"] ?? Environment.GetEnvironmentVariable("EnvoyDirectory"));
            SQLServer = config["SQLServer"] ?? Environment.GetEnvironmentVariable("SQLServer");
            MasterDBName = config["MasterDBName"] ?? Environment.GetEnvironmentVariable("MasterDBName");
            DataDBName = config["DataDBName"] ?? Environment.GetEnvironmentVariable("DataDBName");
            EnctlDBName = config["EnctlDBName"] ?? Environment.GetEnvironmentVariable("EnctlDBName");
            DBUser = config["DBUser"] ?? Environment.GetEnvironmentVariable("DBUser");
            DBPassword = config["DBPassword"] ?? Environment.GetEnvironmentVariable("DBPassword");
            Username = config["Username"] ?? Environment.GetEnvironmentVariable("Username");
            Password = config["Password"] ?? Environment.GetEnvironmentVariable("Password");
            InputDirectory = new DirectoryInfo(config["InputDirectory"] ?? Environment.GetEnvironmentVariable("InputDirectory"));
            DBContext = new DatabaseContext(SQLServer, MasterDBName, DBUser, DBPassword);
            Translations = new Translations(DBContext.Connection, Language);
        }

        private string GetStandardConfigLocation()
        {
            return GetHostCacheLocation();
        }

        public static string GetHostCacheLocation()
        {
            return Path.Combine
            (
                Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                @"PDICache"
            );
        }

        public static string GetStoreCacheLocation()
        {
            return Path.Combine
            (
                Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                @"PDI\Cache"
            );
        }
    }
}